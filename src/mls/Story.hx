/*
 * SPDX-FileCopyrightText: © 2023 jordi fita mas <jfita@peritasoft.com>
 * SPDX-License-Identifier: GPL-3.0-only
 */

package mls;

import h2d.Bitmap;
import hxd.res.Sound;

#if !macro
@:build(mls.res.Config.init())
#end
class Story extends hxd.App {
  public var atlas:mls.res.Atlas;
  public var defaultFont:h2d.Font;
  public var defaultTextColor = 0xffffff;
  public var defaultBubbleColor = 0xff000000;
  public var luminaire:Bitmap;

  override function loadAssets(onLoaded:Void->Void) {
#if (hl && debug)
    hxd.Res.initLocal();
    onLoaded();
#else
    new hxd.fmt.pak.Loader(s2d, onLoaded);
#end
  }

  override function init() {
    super.init();
    s2d.camera.clipViewport = true;
    luminaire = new Bitmap(h2d.Tile.fromColor(engine.backgroundColor));
    luminaire.width = s2d.width;
    luminaire.height = s2d.height;
    s2d.add(luminaire, 1);
  }

  public function shiftScene(s:Scene) {
    scene?.remove();
    scene = s;
    scene.story = this;
    s2d.add(scene, 0);
    s2d.camera.x = 0;
  }

  public function centerAt(w:Float, x:Float): Int {
    return -Std.int(Math.max(0, Math.min(w - s2d.width, x - s2d.width / 2)));
  }

  public function playBgm(song:Sound) {
    if (song == bgm) {
      return;
    }
    if (bgm != null) {
      bgm.stop();
    }
    bgm = song;
    if (bgm != null) {
      bgm.play(true);
    }
  }

  override function update(dt:Float) {
    scene?.update(dt);
  }

  var scene:Scene;
  var bgm:Null<Sound>;
}

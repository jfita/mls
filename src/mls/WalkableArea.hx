/*
 * SPDX-FileCopyrightText: © 2023 jordi fita mas <jfita@peritasoft.com>
 * SPDX-License-Identifier: GPL-3.0-only
 */
package mls;
import h2d.col.Point;
import h2d.col.IPoint;
import h2d.col.Polygon;
import h2d.col.Polygons;
import hxd.Math;

class Edge {
  public function new(to:Int, cost:Float) {
    this.to = to;
    this.cost = cost;
  }

  public function toString() {
    return "{" + to + "," + cost + "}";
  }

  public final to:Int;
  public final cost:Float;
}

class WalkableArea {
  public function new(?points) {
    addNode(pt());
    addNode(pt());
    addPolygon(new Polygon(points), true);
  }

  public inline function iterator() return polygons.iterator();

  public static inline function pt(x = 0, y = 0) return new Point(x, y);
  public static inline function ipt(x = 0, y = 0) return new IPoint(x, y);

  public function addHole(points) {
    addPolygon(new Polygon(points), false);
  }

  public function findPath(from:Point, to:Point):Array<IPoint> {
    final base = polygons[0];
    if (!base.contains(from)) {
      moveToClosestEdge(base, from);
      if (!base.contains(from)) {
	moveCloseToCentroid(base, from);
      }
    }
    if (!base.contains(to)) {
      moveToClosestEdge(base, to);
      if (!base.contains(to)) {
	moveCloseToCentroid(base, to);
      }
    }
    for (i in 1...polygons.length) {
      final hole = polygons[i];
      if (hole.contains(to)) {
	moveToClosestEdge(hole, to);
      }
    }

    nodes[0].load(from);
    nodes[1].load(to);
    clearEdgesOfNode(0);
    clearEdgesOfNode(1);
    final length = nodes.length;
    for (i1 in 0...2) {
      final p1 = nodes[i1];
      for (i2  in (i1+1)...length) {
	final p2 = nodes[i2];
	if (inLineOfSight(p1, p2)) {
	  addEdge(i1, i2, p1.distance(p2));
	}
      }
    }

    return hasEdge(0, 1) ? [to.toIPoint(), from.toIPoint()] : aStar(0, 1);
  }

  function addNode(n:Point) {
    nodes.push(n);
    edges.push([]);
  }

  function addPolygon(polygon:Polygon, first = false) {
    polygons.push(polygon);

    var added = false;
    var p1 = polygon[polygon.length - 2];
    var p2 = polygon[polygon.length - 1];
    for (i in 0...polygon.length) {
      final p3 = polygon[i];
      if (isConcave(p1, p2, p3) == first) {
	addNode(p2);
	added = true;
      }
      p1 = p2;
      p2 = p3;
    }

    if (!added) {
      return;
    }
    clearEdges();
    final length = nodes.length;
    for (i1 in 2...length) {
      final p1 = nodes[i1];
      for (i2 in 2...length) {
	final p2 = nodes[i2];
	if (inLineOfSight(p1, p2)) {
	  addEdge(i1, i2, p1.distance(p2));
	}
      }
    }
  }

  inline function isConcave(p1:Point, p2:Point, t:Point) {
    return (p2.x - p1.x) * (t.y - p1.y) - (p2.y - p1.y) * (t.x - p1.x) < 0;
  }

  inline function clearEdges() {
    for (e in edges) {
      e.resize(0);
    }
  }

  function clearEdgesOfNode(index:Int) {
    for (e in edges[index]) {
      final other = edges[e.to];
      for (i in 0...other.length) {
	if (other[i].to == index) {
	  other.splice(i, 1);
	  break;
	}
      }
    }
    edges[index].resize(0);
  }

  function inLineOfSight(p1:Point, p2:Point): Bool {
    if (!polygons[0].contains(p1)) {
      return false;
    }
    if (!polygons[0].contains(p2)) {
      return false;
    }

    if (p1.distanceSq(p2) < 0.5 * 0.5) {
      return false;
    }

    for (polygon in polygons) {
      var p3 = polygon[polygon.length - 1];
      for (p4 in polygon) {
	if (segmentsIntersect(p1, p2, p3, p4)) {
	  return false;
	}
	p3 = p4;
      }
    }

    tmp.set((p1.x + p2.x) / 2, (p1.y + p2.y) / 2);
    if (!polygons[0].contains(tmp)) {
      return false;
    }
    for (i in 1...polygons.length) {
      if (polygons[i].contains(tmp)) {
	return false;
      }
    }

    return true;
  }

  function segmentsIntersect(a:Point, b:Point, c:Point, d:Point) {
    final denominator = (b.x - a.x) * (d.y - c.y) - (b.y - a.y) * (d.x - c.x);
    if (denominator == 0) {
      return false;
    }

    final numerator1 = (a.y - c.y) * (d.x - c.x) - (a.x - c.x) * (d.y - c.y);
    if (numerator1 == 0) {
      return false;
    }
    final numerator2 = (a.y - c.y) * (b.x - a.x) - (a.x - c.x) * (b.y - a.y);
    if (numerator2 == 0) {
      return false;
    }

    final r = numerator1 / denominator;
    final s = numerator2 / denominator;

    return r > 0 && r < 1 && s > 0 && s < 1;
  }

  function addEdge(from:Int, to:Int, cost:Float) {
    if (!hasEdge(from, to)) {
      edges[from].push(new Edge(to, cost));
    }
    if (!hasEdge(to, from)) {
      edges[to].push(new Edge(from, cost));
    }
  }

  function hasEdge(from:Int, to:Int): Bool {
    for(edge in edges[from]) {
      if (edge.to == to) {
	return true;
      }
    }
    return false;
  }

  function moveCloseToCentroid(pol:Polygon, p:Point) {
    tmp.load(pol.centroid());
    tmp.set(tmp.x - p.x, tmp.y - p.y);
    tmp.normalize();
    p.set(p.x + tmp.x * 0.05, p.y + tmp.y * 0.05);
  }

  function moveToClosestEdge(pol:Polygon, p3:Point) {
    final proj = pol.projectPoint(p3);
    if (proj == null) {
      return;
    }
    p3.set(proj.x, proj.y);
  }

  function aStar(start:Int, goal:Int):Array<IPoint> {
    final goalPoint = nodes[goal];

    final cameFrom = new Map<Int, Int>();
    final gCost = new Array<Float>();
    final fCost = new Array<Float>();

    for (i in 0...nodes.length) {
      gCost[i] = Math.POSITIVE_INFINITY;
      fCost[i] = Math.POSITIVE_INFINITY;
    }
    gCost[start] = 0.;
    fCost[start] = nodes[start].distance(goalPoint);

    function reconstructPath(current:Int):Array<IPoint> {
      final path = [nodes[current].toIPoint()];
      while (cameFrom.exists(current)) {
	current = cameFrom[current];
	path.push(nodes[current].toIPoint());
      }
      return path;
    }

    final openSet = new IndexedPriorityQueue(fCost);
    openSet.insert(start);
    while (openSet.length > 0) {
      final current = openSet.pop();
      if (current == goal) {
	return reconstructPath(current);
      }
      for (edge in edges[current]) {
	final tentativeGCost = gCost[current] + edge.cost;
	if (tentativeGCost < gCost[edge.to]) {
	  cameFrom[edge.to] = current;
	  gCost[edge.to] = tentativeGCost;
	  fCost[edge.to] = tentativeGCost + nodes[edge.to].distance(goalPoint);
	  openSet.insert(edge.to);
	}
      }
    }

    return [];
  }

  public function drawDebug(g:h2d.Graphics) {
    for (polygon in this) {
      g.beginFill(0x0000FF);
      for (point in polygon) {
	g.drawCircle(point.x, point.y, 4);
      }
      g.endFill();

      g.lineStyle(1, 0x0000FF);
      var prev:Null<Point> = null;
      for (point in polygon) {
	if (prev == null) {
	  g.moveTo(point.x, point.y);
	} else {
	  g.lineTo(point.x, point.y);
	}
	prev = point;
      }
      if (prev != null) {
	final first = polygon[0];
	g.lineTo(first.x, first.y);
      }
    }

    g.beginFill(0xFFFF00);
    for (point in nodes) {
      g.drawCircle(point.x, point.y, 4);
    }
    g.endFill();

    g.lineStyle(1, 0x00FF00, .2);
    for (from in 0...edges.length) {
      final p0 = nodes[from];
      final edges = edges[from];
      for (edge in edges) {
	final p1 = nodes[edge.to];
	g.moveTo(p0.x, p0.y);
	g.lineTo(p1.x, p1.y);
      }
    }
  }

  final polygons = new Polygons();
  final tmp = new h2d.col.Point();
  final nodes = new Array<Point>();
  final edges = new Array<Array<Edge>>();
}

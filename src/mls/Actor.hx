/*
 * SPDX-FileCopyrightText: © 2023 jordi fita mas <jfita@peritasoft.com>
 * SPDX-License-Identifier: GPL-3.0-only
 */

package mls;

import h2d.col.IPoint;
import h2d.col.Point;
import mls.WalkableArea.ipt;

enum Face {
  Right;
  Left;
}

class Actor extends AtlasAnim {
  public var walkSpeed = 150.;
  public var textPos = -250;
  public var textColor: Null<Int>;
  public var bubbleColor: Null<Int>;
  public var face(default, set) = Right;
  public var idleAnim(default, set) = "idle";
  public var talkAnim(default, set) = "talk";
  public var ix(get, never): Int;
  public var iy(get, never): Int;

  public function new(atlas:mls.res.Atlas, name:String, ?parent:h2d.Object) {
    this.name = name;
    super(atlas, idleAnim, 7, parent);
  }

  override function onAdd() {
    super.onAdd();
    scene = cast(parent, Scene);
  }

  override function playAnim(anim:String, atFrame = 0.0) {
    super.playAnim(anim, atFrame);
    if (face == Left) {
      flipX();
    }
  }

  public function waitAnim(anim:String, atFrame = 0.0):Coroutine {
    return new WaitAnimCoroutine(this, anim, atFrame);
  }

  override function getFrames(anim:String) {
    return super.getFrames(name + "_" + anim);
  }

  public function walkTo(x:Int, y:Int):Coroutine {
    return walkPath(scene.findPath(ix, iy, x, y));
  }

  public function walkToMouse(?dx = 0, ?dy = 0):Coroutine {
    return walkPath(scene.findPathToMouse(ix, iy, dx, dy));
  }

  public function walkStraightTo(x:Int, y:Int):Coroutine {
    return walkPath([ipt(x, y), ipt(ix, iy)]);
  }

  public function walkPath(path: Array<IPoint>):Coroutine {
    return new WalkCoroutine(this, path);
  }

  public function say(what:String):Coroutine {
    return new SayCoroutine(this, what, scene.story);
  }

  inline public function faceTowards(o:h2d.Object) {
    face = o.x < x ? Left : Right;
  }

  inline function get_ix() return Std.int(x);
  inline function get_iy() return Std.int(y);

  function set_idleAnim(newAnim:String) {
    if (idleAnim != newAnim && anim == idleAnim) {
      playAnim(newAnim, currentFrame);
    }
    return idleAnim = newAnim;
  }

  function set_talkAnim(newAnim:String) {
    if (talkAnim != newAnim && anim == talkAnim) {
      playAnim(newAnim, currentFrame);
    }
    return talkAnim = newAnim;
  }

  function set_face(newFace) {
    if (face != newFace) {
      flipX();
    }
    return face = newFace;
  }

  var scene:Scene;
}

private class WalkCoroutine implements Coroutine {
  public function new(actor:Actor, path:Array<IPoint>) {
    this.actor = actor;
    this.path = path;
    dest = path.pop();
    pos.set(actor.x, actor.y);
    actor.playAnim("walk");
  }

  public function run(dt:Float):mls.Coroutine.Result {
    vel.set(dest.x - pos.x, dest.y - pos.y);
    final distance = vel.length();
    final walkDistance = actor.walkSpeed * dt;
    vel.normalize();
    vel.scale(Math.min(distance, walkDistance));
    pos.x += vel.x;
    pos.y += vel.y;
    actor.setPosition(Std.int(pos.x), Std.int(pos.y));
    if (actor.x == dest.x && actor.y == dest.y) {
      if (path.length > 0) {
	dest = path.pop();
      } else {
	actor.playAnim(actor.idleAnim);
	return Halt;
      }
    }
    return NextFrame;
  }

  inline function set_dest(point:IPoint) {
    actor.face = point.x < actor.x ? Left : point.x > actor.x ? Right : actor.face;
    return dest = point;
  }

  final actor:Actor;
  final path:Array<IPoint>;
  final vel = new Point();
  final pos = new Point();
  var dest (default, set):Null<IPoint> = null;
}

private class SayCoroutine implements Coroutine {
  public function new(actor:Actor, what:String, story:Story) {
    this.actor = actor;
    this.story = story;
    speech = new Speech(actor.textPos, what, actor, story);
    actor.playAnim(actor.talkAnim);
    story.s2d.addEventListener(handleEvent);
  }

  public function run(dt:Float):mls.Coroutine.Result {
    if (time > 0) {
      time -= dt;
      return NextFrame;
    }
    if (!done) {
      return NextFrame;
    }
    story.s2d.removeEventListener(handleEvent);
    actor.playAnim(actor.idleAnim);
    speech.remove();
    return Halt;
  }

  function handleEvent(event:hxd.Event) {
    switch(event.kind) {
    case EPush:
      if (time <= 0) {
	done = true;
      }
    default:
    }
  }

  final actor:Actor;
  final story:Story;
  final speech:Speech;
  var time = .25;
  var done = false;
}

private class WaitAnimCoroutine implements Coroutine {
  public function new(actor:Actor, anim:String, atFrame:Float) {
    this.actor = actor;
    actor.loop = false;
    prevOnAnimEnd = actor.onAnimEnd;
    actor.onAnimEnd = finish;
    actor.playAnim(anim, atFrame);
  }

  function finish() {
    done = true;
  }

  public function run(dt:Float):mls.Coroutine.Result {
    if (!done) {
      return NextFrame;
    }
    actor.onAnimEnd = prevOnAnimEnd;
    actor.loop = true;
    actor.playAnim(actor.idleAnim);
    return Halt;
  }

  final actor:Actor;
  final prevOnAnimEnd:()->Void;
  var done = false;
}

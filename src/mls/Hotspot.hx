/*
 * SPDX-FileCopyrightText: © 2023 jordi fita mas <jfita@peritasoft.com>
 * SPDX-License-Identifier: GPL-3.0-only
 */
package mls;
import h2d.Bitmap;
import hxd.Event;

class Hotspot extends h2d.Interactive {
  public function new(width:Float, height:Float, story:Story, ?parent:h2d.Object) {
    super(width, height, parent);
    this.story = story;
    #if debug
    backgroundColor = 0x22ff0000;
    #end
  }

  override function onOver(e:Event) {
    if (label == null) {
      final text = new h2d.Text(story.defaultFont);
      text.textColor = 0x000000;
      text.text = name;
      text.setPosition(10, 0);
      text.smooth = true;
  
      final background = h2d.Tile.fromColor(0xffffff);
      label = new Bitmap(background);
      label.width = Std.int(text.textWidth) + 20;
      label.height = Std.int(text.textHeight) + 10;
      label.addChild(text);

      story.s2d.add(label, 2);
    }
    moveName(e.relX, e.relY);
  }

  override function onMove(e:Event) {
    moveName(e.relX, e.relY);
  }

  function moveName(x:Float, y:Float) {
    if (label != null) {
      label.visible = true;
      tmp.set(x - label.width / 2, y - label.height - 30);
      this.localToGlobal(tmp);
      if (tmp.y < 0) {
	tmp.set(x - label.width / 2, y + 30);
	this.localToGlobal(tmp);
      }
      label.x = Std.int(tmp.x);
      label.y = Std.int(tmp.y);
    }
  }

  override function onOut(e:Event) {
    if (label != null) {
      label.visible = false;
    }
  }

  override function onRemove() {
    super.onRemove();
    label.remove();
    label = null;
  }

  override function set_visible(b) {
    super.set_visible(b);
    if (label != null && !b) {
      label.visible = false;
    }
    return b;
  }

  public function rename(n:String) {
    if (label != null) {
      label.remove();
      label = null;
    }
    name = n;
    return null;
  }

  final story:Story;
  var label:Bitmap;
  final tmp = new h2d.col.Point();
}
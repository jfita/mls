/*
 * SPDX-FileCopyrightText: © 2023 jordi fita mas <jfita@peritasoft.com>
 * SPDX-License-Identifier: GPL-3.0-only
 */

package mls;

import mls.res.Atlas;
import h2d.Flow;

class AtlasAnim extends h2d.Anim {
  public var hotspotX (get, null):Int;
  public var hotspotY (get, null):Int;
  public var hotspotW (get, null):Int;
  public var hotspotH (get, null):Int;

  public function new(atlas:Atlas, name:String, speed:Float, ?parent:h2d.Object, ?horizontalAlign = FlowAlign.Middle, ?verticalAlign = FlowAlign.Bottom) {
    this.atlas = atlas;
    anim = name;
    halign = horizontalAlign;
    valign = verticalAlign;
    super([], speed, parent);
    reload();
  }

  override function onAdd() {
    super.onAdd();
    atlas.addWatch(reload);
  }

  override function onRemove() {
    super.onRemove();
    atlas.removeWatch(reload);
  }

  function reload() {
    if (anim == null) {
      return;
    }
    playAnim(anim, currentFrame);
  }

  public function playAnim(name:String, atFrame = 0.0) {
    anim = name;
    final frames = getFrames(anim);
    play(frames, Math.min(frames?.length ?? 0, atFrame));
  }

  function getFrames(name:String) {
    return atlas.getAnim(name, halign, valign);
  }

  public function flipX() {
    for (frame in frames) {
      frame?.flipX();
    }
  }

  function get_hotspotX() return Std.int(x - (hotspotW >> 1));
  function get_hotspotY() return Std.int(y - hotspotH);
  function get_hotspotW() return getFrame().iwidth;
  function get_hotspotH() return getFrame().iheight;

  final atlas:Atlas;
  var anim:String;
  var halign:FlowAlign;
  var valign:FlowAlign;
}

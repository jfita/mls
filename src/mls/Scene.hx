/*
 * SPDX-FileCopyrightText: © 2023 jordi fita mas <jfita@peritasoft.com>
 * SPDX-License-Identifier: GPL-3.0-only
 */
package mls;
import mls.WalkableArea.pt;
import h2d.Object;
import h2d.col.IPoint;
import hxd.res.Sound;

@:autoBuild(mls.SceneMacros.script())
class Scene extends h2d.Layers {
  public var bgm(never, set):Sound;

  public function new (name:String, ?parent:Object) {
    super(parent);
    this.name = name;
  }

  override function onAdd() {
    super.onAdd();
    backdrop = new AtlasBitmap(story.atlas, '${name}_backdrop', this);
    #if debug
    add(g);
    #end
  }

  override function onRemove() {
    removeChildren();
    super.onRemove();
  }

  public function update(dt:Float) {
    thread.run(dt);
    if (ego != null && backdrop != null) {
      x = story.centerAt(backdrop.tile.width, ego.x);
    }
    #if debug
    g.clear();
    walkable.drawDebug(g);
    #end
    ysort(0);
  }

  function wait(s:Float): Coroutine {
    return new WaitCoroutine(s);
  }

  inline public function findPath(fx:Int, fy:Int, tx:Int, ty:Int):Array<IPoint> {
    return walkable.findPath(pt(fx, fy), pt(tx, ty));
  }

  inline public function findPathToMouse(fx:Int, fy:Int, dx:Int, dy:Int):Array<IPoint> {
    return findPath(fx, fy, Std.int(story.s2d.mouseX + dx - x), Std.int(story.s2d.mouseY + dy));
  }

  inline function fadeIn(?duration = 1.0) return fade(1, 0, duration);
  inline function blackout(?duration = 1.0) return fade(0, 1, duration);

  inline function fade(from:Float, to:Float, ?duration = 1.0):Coroutine {
    return new FadeCoroutine(from, to, duration, story.luminaire);
  }

  inline function parallel(...inner:Coroutine):Coroutine {
    return new ParallelCoroutine(inner);
  }

  overload extern inline public function addHotspot(a:AtlasAnim, name:String, ?parent:Object = this) {
    return addHotspot(a.hotspotX, a.hotspotY, a.hotspotW, a.hotspotH, name, parent);
  }

  overload extern inline public function addHotspot(x:Int, y:Int, w:Int, h:Int, name:String, ?parent:Object = this) {
    final hs = new Hotspot(w, h, story, parent);
    hs.setPosition(x, y);
    hs.name = name;
    hotspots.push(hs);
    lastHotspot = hs;
    return hs;
  }

  function whenInteract(f:Void->Void) {
    if (lastHotspot == null) return;
    lastHotspot.onClick = (_) -> f();
  }

  function blockHotspots() {
    for (hs in hotspots) {
      hs.visible = false;
    }
  }

  function activateHotspots() {
    for (hs in hotspots) {
      hs.visible = true;
    }
  }

  function removeAllHotspots() {
    while (hotspots.length > 0) {
      hotspots.pop().remove();
    }
  }

  function shiftScene(s:Scene) {
    story.shiftScene(s);
  }

  inline function set_bgm(song:Sound):Sound {
    story.playBgm(song);
    return song;
  }

  public var story:Story;
  var walkable = new WalkableArea();
  final thread = new Coroutine.Thread();
  final hotspots = new Array<Hotspot>();
  var lastHotspot:Hotspot;
  var backdrop:AtlasBitmap;
  var ego:Actor;
  #if debug
  final g = new h2d.Graphics();
  #end
}

private class WaitCoroutine implements Coroutine {
  public function new(s:Float) {
    this.s = s;
  }

  public function run(dt:Float):mls.Coroutine.Result {
    s -= dt;
    return s > 0 ? NextFrame : Halt;
  }

  var s:Float;
}

private class FadeCoroutine implements Coroutine {
  public function new(from:Float, to:Float, duration:Float, luminaire:Object) {
    this.from = from;
    this.to = to;
    this.duration = duration;
    this.luminaire = luminaire;
    luminaire.alpha = from;
  }

  public function run(dt:Float):mls.Coroutine.Result {
    t += dt;
    luminaire.alpha = hxd.Math.lerp(from, to, Math.min(1., t / duration));
    return t < duration ? NextFrame : Halt;
  }

  final from:Float;
  final to:Float;
  final duration:Float;
  final luminaire:Object;
  var t = 0.0;
}

private class ParallelCoroutine implements Coroutine {
  public function new(coroutines: Array<Coroutine>) {
    this.inner = coroutines;
  }

  public function run(dt:Float):mls.Coroutine.Result {
    var result = mls.Coroutine.Result.Halt;
    for (i in 0...inner.length) {
      final c = inner[i];
      if (c == null) continue;
      switch(c.run(dt)) {
      case NextFrame:
	result = NextFrame;
      case Call(o):
	return Call(o);
      case TailCall(o):
	inner[i] = o;
      case Halt:
	inner[i] = null;
      }
    }
    return result;
  }

  final inner:Array<Coroutine>;
}

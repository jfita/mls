/*
 * SPDX-FileCopyrightText: © 2023 jordi fita mas <jfita@peritasoft.com>
 * SPDX-License-Identifier: GPL-3.0-only
 */

package mls.res;

class Atlas extends hxd.res.Atlas {
  override function getContents() {
    if (contents == null) {
      watch(watchCallback);
    }
    return super.getContents();
  }

  function watchCallback() {
    contents = null;
    for (callback in watchCallbacks) {
      callback();
    }
  }

  public function addWatch(callback:Void->Void) {
    watchCallbacks.push(callback);
  }

  public function removeWatch(callback:Void->Void) {
    watchCallbacks.remove(callback);
  }

  final watchCallbacks:Array<Void->Void> = [];
}

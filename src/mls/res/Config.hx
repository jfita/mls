/*
 * SPDX-FileCopyrightText: © 2023 jordi fita mas <jfita@peritasoft.com>
 * SPDX-License-Identifier: GPL-3.0-only
 */

package mls.res;
#if macro
import haxe.macro.Context;

class Config {
  public static function init() {
    hxd.res.Config.addExtension("atlas", "mls.res.Atlas");
    return Context.getBuildFields();
  }
}
#end

/*
 * SPDX-FileCopyrightText: © 2023 jordi fita mas <jfita@peritasoft.com>
 * SPDX-License-Identifier: GPL-3.0-only
 */

package mls;

enum Result {
  NextFrame;
  Halt;
  Call(co:Coroutine);
  TailCall(co:Coroutine);
}

interface Coroutine {
  public function run(dt:Float):Result;
}

@:forward(isEmpty, add)
abstract Thread(haxe.ds.GenericStack<Coroutine>) {
  inline public function new() {
    this = new haxe.ds.GenericStack<Coroutine>();
  }

  public function run(dt:Float) {
    final c = this.first();
    if (c == null) {
      return;
    }
    switch (c?.run(dt)) {
    case NextFrame: // nothing to do
    case Call(co):
      this.add(co);
      run(dt);
    case TailCall(co):
      this.pop();
      this.add(co);
      run(dt);
    case Halt:
      this.pop();
    }
  }
}
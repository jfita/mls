/*
 * SPDX-FileCopyrightText: © 2023 jordi fita mas <jfita@peritasoft.com>
 * SPDX-License-Identifier: GPL-3.0-only
 */
package mls;

class IndexedPriorityQueue
{
  public var length(get, never):Int;

  public function new(priorities:Array<Float>) {
    this.priorities = priorities;
  }

  public function insert(index:Int) {
    if (!indices.contains(index)) {
      indices.push(index);
    }
    reorderUp();
  }

  public function pop(): Int {
    final index = indices[0];
    indices[0] = indices[indices.length - 1];
    indices.pop();
    reorderDown();
    return index;
  }

  function reorderUp() {
    var i = indices.length - 1;
    while (i > 0) {
      if (priorities[indices[i]] < priorities[indices[i - 1]]) {
	final tmp = indices[i];
	indices[i] = indices[i - 1];
	indices[i - 1] = tmp;
      } else {
	break;
      }
      i--;
    }
  }

  function reorderDown() {
    for (i in 0...indices.length - 1) {
      if (priorities[indices[i]] > priorities[indices[i + 1]]) {
	final tmp = indices[i];
	indices[i] = indices[i + 1];
	indices[i + 1] = tmp;
      } else {
	break;
      }
    }
  }

  inline function get_length() return indices.length;

  final priorities:Array<Float>;
  final indices = new Array<Int>();
}
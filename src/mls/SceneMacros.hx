/*
 * SPDX-FileCopyrightText: © 2023 jordi fita mas <jfita@peritasoft.com>
 * SPDX-License-Identifier: GPL-3.0-only
 */
package mls;
import haxe.macro.Context;
import haxe.macro.Expr;
import haxe.macro.Type;
using StringTools;
using haxe.macro.TypeTools;
using haxe.macro.ComplexTypeTools;

class SceneMacros {
  public static macro function script():Array<Field> {
    final localType = Context.getLocalType();
    var scopedVars = [];
    scopedVars.push({type: localType.toComplexType(), name: "_this"});
    findScopedVars(localType.getClass(), scopedVars);
    final fields = Context.getBuildFields();
    for (field in fields) {
      switch (field.kind) {
      case FVar(type, _):
	scopedVars.push({type: type, name: field.name});
      case FProp(_, _, type, _):
	scopedVars.push({type: type, name: field.name});
      case FFun(_):
      }
    }
    for (field in fields) {
      switch(field.kind) {
      case FFun(_f):
	parseExpr(_f.expr, scopedVars);
      case FProp(_, _, _, _) | FVar(_, _):
      }
    }
    return fields;
  }

  static function findScopedVars(c:ClassType, scopedVars: Array<Var>) {
    if (c.superClass != null) {
      findScopedVars(c.superClass.t.get(), scopedVars);
    }
    for (field in c.fields.get()) {
      switch (field.kind) {
      case FVar(_, _):
	scopedVars.push({type: field.type.toComplexType(), name: field.name});
      default:
      }
    }
  }

  static function parseExpr(expr:Null<Expr>, scopedVars:Array<Var>) {
    if (expr == null) {
      return;
    }
    switch (expr.expr) {
    case EBlock(exprs):
      parseBlock(exprs, scopedVars);
    case ESwitch(_, cases, _):
      for (c in cases) {
	parseExpr(c.expr, scopedVars);
      }
    case EIf(_, eif, eelse):
      parseExpr(eif, scopedVars);
      parseExpr(eelse, scopedVars);
    case ECall(e, params):
      parseExpr(e, scopedVars);
      for (param in params) {
	parseExpr(param, scopedVars);
      }
    case EFunction(_, f):
      parseExpr(f.expr, scopedVars);
    case EReturn(e):
      parseExpr(e, scopedVars);
    case EMeta(_, e):
      parseExpr(e, scopedVars);
    default:
    }
  }

  static function parseBlock(block:Array<Expr>, scopedVars: Array<Var>) {
    for (i in 0...block.length) {
      final e = block[i];
      switch (e.expr) {
      case EMeta(_s, _e):
	if (_s.name == ":script") {
	  block[i] = switch (_e.expr) {
	  case EBlock(body): buildCoroutine(body, _s.pos, scopedVars);
	  default: _e;
	  }
	}
      default:
	parseExpr(e, scopedVars);
      }
    }
  }

  static function buildCoroutine(block:Array<Expr>, pos:Position, scopedVars): Expr {
    if (block.length == 1) {
      return macro thread.add(${block[0]});
    }

    final name = "Coroutine_script_num_" + Std.int(Math.random() *  2147483647);
    var c = macro class $name implements mls.Coroutine {
      public function new(fn:Int->mls.Coroutine.Result) {
	this.fn = fn;
      }

      public function run(dt:Float): mls.Coroutine.Result {
	final result = fn(state);
	state++;
	return result;
      }

      final fn:Int->mls.Coroutine.Result;
      var state = 0;
    };
    Context.defineType(c);

    var currentState = 0;
    var stateExprs = new Array<Expr>();
    final states = new Array<Case>();
    for (i in 0...block.length) {
      final e = block[i];
      if (isCoroutineCall(e, scopedVars)) {
	stateExprs.push(i == block.length - 1 ? macro return TailCall(${e}) : macro return Call(${e}));
	states.push({values: [macro $v{currentState}], expr: macro { $a{stateExprs} }});
	currentState++;
	stateExprs = [];
      } else {
	stateExprs.push(e);
      }
    }
    if (stateExprs.length > 0) {
	stateExprs.push(macro return Halt);
	states.push({values: [macro $v{currentState}], expr: macro { $a{stateExprs} }});
    }

    final stateSwitch = { expr: ESwitch(macro $i{'_state'}, states, null), pos: pos };
    final typePath = {pack: [], name: name}
      return macro thread.add(new $typePath(((_state:Int) -> {
	      ${stateSwitch}
	      return Halt;
	    })));
  }

  static function isCoroutineCall(expr:Expr, scopedVars:Array<Var>): Bool {
    switch (expr.expr) {
    case ECall(inner, params):
      switch (inner.expr) {
      case EConst(CIdent(field)):
	expr = macro _this.$field($a{params});
      default:
      }
      final localVars = {pos: expr.pos, expr: EVars(scopedVars)};
      final type = Context.typeof(macro {${localVars} ${expr}});
      return type.unify((macro : mls.Coroutine).toType());
    case ENew(type, params):
      return true;
    default:
      return false;
    }
  }
}

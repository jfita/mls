/*
 * SPDX-FileCopyrightText: © 2023 jordi fita mas <jfita@peritasoft.com>
 * SPDX-License-Identifier: GPL-3.0-only
 */

package mls;

import mls.res.Atlas;
import h2d.Flow;

class AtlasBitmap extends h2d.Bitmap {
  public function new(atlas:Atlas, tileName:String, ?parent:h2d.Object, halign = FlowAlign.Left, valign = FlowAlign.Top) {
    this.atlas = atlas;
    this.tileName = tileName;
    this.halign = halign;
    this.valign = valign;
    super(getTile(), parent);
  }

  inline function getTile() return atlas.get(tileName, halign, valign);

  override function onAdd() {
    super.onAdd();
    atlas.addWatch(reload);
  }

  override function onRemove() {
    super.onRemove();
    atlas.removeWatch(reload);
  }

  function reload() {
    tile = getTile();
  }

  final atlas:Atlas;
  final tileName:String;
  final halign:FlowAlign;
  final valign:FlowAlign;
}

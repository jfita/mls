/*
 * SPDX-FileCopyrightText: © 2023 jordi fita mas <jfita@peritasoft.com>
 * SPDX-License-Identifier: GPL-3.0-only
 */

package mls;

import h2d.Object;
import h2d.Text;

class Speech extends Object {
  public function new(y:Float, line:String, actor:Actor, story:Story) {
    super(story.s2d);

    final t = actor.localToGlobal();
    t.y += y * story.s2d.scaleY;
    this.globalToLocal(t);
    this.y = t.y;
    this.x = t.x;

    final tail = new h2d.Bitmap(story.atlas.get("speech_tail"), this);
    tail.color.setColor(actor.bubbleColor ?? story.defaultBubbleColor);
    tail.setPosition(-tail.tile.width / 2, -tail.tile.height);

    final bubble = new h2d.ScaleGrid(story.atlas.get("speech_bubble"), 32, 32, 32, 32, tail);

    final text = new Text(story.defaultFont, bubble);
    text.maxWidth = 400;
    text.setPosition(32, 0);
    text.textColor = actor.textColor ?? story.defaultTextColor;
    text.letterSpacing = 0;
    text.smooth = true;
    text.textAlign = Align.MultilineCenter;
    text.text = line;

    bubble.color.load(tail.color);
    bubble.width = text.textWidth + 64;
    bubble.height = Math.max(64, text.textHeight + 12);
    text.y = bubble.height / 2 - text.textHeight / 2 - 6;
    bubble.setPosition(-tail.x - bubble.width / 2, -bubble.height);

    final p = bubble.localToGlobal();
    if (p.x < 5) {
      bubble.x -= p.x - 5;
    } else if ((p.x + bubble.width) > story.s2d.width - 5) {
      bubble.x -= p.x + bubble.width - story.s2d.width + 5;
    }
    if (p.y < 5) {
      tail.y -= p.y - 5;
    }
  }
}

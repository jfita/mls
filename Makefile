NULL = 

PROJECT = example
WINDOW_SIZE = 940x540
WINDOW_TITLE = Point and Click Adventure

SHEETS = \
	$(NULL)

ATLAS_TEXTURES := $(patsubst %,res/%.png,$(SHEETS))
ATLAS := #res/tiles.atlas

FONTS = \
	$(NULL)

FONT_TEXTURES := $(patsubst %,res/%.png,$(FONTS))
FONT_FNT := $(patsubst %.png,%.fnt,$(FONT_TEXTURES))

RESOURCES = \
	$(ATLAS) \
	$(ATLAS_TEXTURES) \
	$(FONT_FNT) \
	$(FONT_TEXTURES) \
	$(NULL)

HAXE_SRC := $(wildcard src/*.hx src/**/*.hx)

HAXE = haxe
HAXEPORT = 6000
HAXEFLAGS = \
	--class-path src \
	--library mls \
	--library heaps \
	--main Main \
	--define no-deprecation-warnings \
	$(NULL)
HAXEJSFLAGS = \
	--dce full \
	$(NULL)
HAXEHLFLAGS = \
	--library hashlink \
	--library hlsdl \
	--define hl_legacy32 \
	--define hot-reload \
	--define heaps_enable_hl_mp3 \
	--define windowSize='$(WINDOW_SIZE)' \
	--define windowTitle="$(WINDOW_TITLE)" \
	$(NULL)

RESPAK = bin/res.pak
RESPAKHL = bin/hxd.fmt.pak.Build.hl
JSBIN = bin/$(PROJECT).js
HLBIN = bin/$(PROJECT).hl

ZIP = zip
ZIPFLAGS = -9 -j
ZIPBIN = bin/$(PROJECT).zip

define haxe_build
$(HAXE) --connect $(HAXEPORT) $(1) || ( [ $$? -eq 2 ] && $(HAXE) $(1) )
endef

$(HLBIN): $(HAXE_SRC) | $(RESOURCES)
	$(call haxe_build,--debug --dce no --hl $@ $(HAXEFLAGS) $(HAXEHLFLAGS))

$(JSBIN): $(HAXE_SRC) | $(RESOURCES) $(RESPAK)
	$(call haxe_build,--js $@ $(HAXEFLAGS) $(HAXEJSFLAGS))

$(ZIPBIN): $(JSBIN) $(RESPAK) bin/index.html
	$(ZIP) $(ZIPFLAGS) $@ $^

$(ATLAS): $(ATLAS_TEXTURES)
	extractatlas $^ $@

res/%.png: assets/%.svg
	IDS=$$(xml select --text --template --match '/svg:svg/svg:g/svg:*' --value-of '@id' --output ';' $<) && \
		inkscape --export-id-only --export-type=png --export-id="$${IDS::${#IDS}-1}" $<
	packtextures assets/$*_*.png $@
	rm assets/$*_*.png

res/%.png res/%.fnt: fonts.json assets/%.ttf
	fontgen $<

$(RESPAK): $(RESPAKHL) $(RESOURCES)
	hl $< -out $(basename $@)

$(RESPAKHL):
	haxe --hl $@ --library heaps --main hxd.fmt.pak.Build

.INTERMEDIATE: $(RESPAKHL)

all: $(HLBIN) dist

dist: $(ZIPBIN)

run: $(HLBIN)
	hl --hot-reload $<

watch:
	while true; do $(MAKE); inotifywait --recursive --event modify assets src Makefile; sleep 0.25; done

listen:
	haxe --server-listen $(HAXEPORT)

serve: | $(JSBIN)
	cd bin && python3 -mhttp.server

clean:
	rm -fr $(HLBIN) $(JSBIN) $(ZIPBIN) $(RESOURCES) $(RESPAK)

.PHONY: all dist run watch listen clean
